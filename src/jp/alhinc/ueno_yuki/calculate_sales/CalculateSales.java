package jp.alhinc.ueno_yuki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		BufferedReader br = null;
		BufferedReader brtwo = null;
		HashMap<String, String> branchmap = new HashMap<String, String>();
		HashMap<String, Long> salesmap = new HashMap<String, Long>();
		try {
			File file = new File(args[0], "branch.lst");
			if(file.exists() == false) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				String[] x = line.split(",");
				if((x.length != 2)||(x[0].matches("[0-9]{3}") == false)) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				branchmap.put(x[0], x[1]);
				salesmap.put(x[0],0L);
			}
			File urifile = new File(args[0]);
			File[] list = urifile.listFiles();//listFilesはファイルの一覧を取得

			List<File> filebox = new ArrayList<>();

			for(int i = 0;i< list.length;i++){
				String filename = list[i].getName();
				if(filename.matches("[0-9]{8}.rcd") == true) {

					filebox.add(list[i]);
				}
			}
			//Faile　eには配列の場所が入っているので要素が入っているわけではない
			File e = filebox.get(filebox.size() - 1);
			int saidai= Integer.parseInt(e.getName().substring(0, 8));

			File f = filebox.get(0);
			int saisyou=Integer.parseInt(f.getName().substring(0, 8));

			if ((saidai - saisyou + 1) != filebox.size()) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
			for(int i = 0;i<filebox.size();i++){
				try {
					String filename = filebox.get(i).getName();


					FileReader frtwo = new FileReader( filebox.get(i));
					brtwo = new BufferedReader(frtwo);

					String sitencode=brtwo.readLine();

					if(salesmap.get(sitencode)==null) {
						System.out.println(filename + "の支店コードが不正です");
						return;
					}
					String konuri=brtwo.readLine();

					if(brtwo.readLine()!=null) {
						System.out.println(filename + "のフォーマットが不正です");
					}
					long sinurione =salesmap.get(sitencode)+ Long.parseLong(konuri);
					String sinuritwo=String.valueOf(sinurione);
					if(sinuritwo.length()>10) {
						System.out.println("合計金額が10桁を超えました");
						return;
					}
					long sinuri = Long.parseLong(sinuritwo);
					salesmap.put(sitencode,sinuri);

				} catch(IOException e1) {
					if(brtwo != null) {
						try{
							brtwo.close();
						} catch (IOException e2) {
							System.out.println("予期せぬエラーが発生しました");
						  }
					}
				}
			}
			BufferedWriter bw = null;
			File file2 = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(file2);
			bw = new BufferedWriter(fw);
			for (Map.Entry<String,Long>entry : salesmap.entrySet()) {//entrySetで salesmapの値をすべて処理していく
				bw.write(entry.getKey() + "," + (branchmap.get(entry.getKey())) + "," + (salesmap.get(entry.getKey())) );
				bw.newLine();
			}
			bw.close();
		}
		catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
		}
		finally {
			 if (br != null) {
				try {
					br.close();
				}
				catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");

				 }
			 }
		 }



	}
}
